$(document).ready(function() {
    var modal = $('#myModal'),
        modalError = $('#myModalError'),
        CurrentDeleteBtn = null,
        BackBtn = null;
    $('#table').DataTable();

    $('.delete-btn').click(function(){
        CurrentDeleteBtn = $(this);
        BackBtn = $(this);
        modal.find('#confirm').attr('data-id', $(this).data('id'));
        modal.find('#back').attr('data-id', $(this).data('id'));
        modal.modal('show');
    });

    $('#confirm').click(function(event){
        event.preventDefault();
        var that = CurrentDeleteBtn,
            id = that.data('id'),
            location = window.location.pathname.split('/').pop();
        sendForDelete(location, id);
    });

    $('#back').click(function(event){
        event.preventDefault();
        var back = BackBtn,
            id = back.data('id');
        location = window.location.pathname.split('/').pop();
        sendForDelete(location, id);
    });
    function sendForDelete(where, id)
    {

        var url = '/admin/'+where+'/'+id;
        $.ajax({
            url: url,
            method: 'DELETE',
            success: function (res) {
                if(res.status){
                    modal.modal('hide');
                    $('tr[data-id='+ id +']').remove();
                }
            },
            error: function (res) {
                modal.modal('hide');
                modalError.modal('show');

            }
        })
    }
});