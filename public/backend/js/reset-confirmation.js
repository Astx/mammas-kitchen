$(document).ready(function() {
    var modal = $('#resetConfirm'),
        row = null;
    $('.reset-btn').click(function(){
        row = $(this);
        modal.find('#reset-reserve').attr('data-id', $(this).data('id'));

        modal.modal('show');
    });

    $('#reset-reserve').click(function(event){
        event.preventDefault();
        var that = row,
        id = that.data('id'),
        url = '/admin/reservation/'+that.data('id');
        $.ajax({
            url: url,
            method: 'POST',
            success: function (res) {
                if(res.status){
                    modal.modal('hide');
                    that.hide();
                    $('.confirm-btn[data-id='+ id +']').show();
                    $('span[data-id='+id+']').removeClass('label-info')
                    .addClass('label-danger')
                    .text('Not Confirmed Yet');
                }
            },
            error: function (res) {
                console.log('Error');
            }
        })
    });
});