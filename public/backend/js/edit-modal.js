$(document).ready(function() {
    var modal = $('#confirmModal'),
        currentConfirmBtn = null;
    $('.confirm-btn').click(function(){
        currentConfirmBtn = $(this);
        modal.find('#confirmation').attr('data-id', $(this).data('id'));
        modal.modal('show');
    });

    $('#confirmation').click(function(event){
        event.preventDefault();
        var that =  currentConfirmBtn,
            id = that.data('id'),
            url = '/admin/reservation/'+id;
        $.ajax({
            url: url,
            method: 'POST',
            success: function (res) {
                if(res.status){
                    modal.modal('hide');
                    that.hide();
                    $('.reset-btn[data-id='+ id +']').show();
                    $('span[data-id='+id+']').removeClass('label-danger')
                        .addClass('label-info')
                        .text('Confirmed');
                }
            },
            error: function (res) {
                console.log('For testing git branch');
            }
        })
    });
});