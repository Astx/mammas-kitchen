<?php

namespace App\Http\Controllers;

use App\Reservation;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reserve(Request $request)
    {

        $data = $request->all();

        $this->validate($request,[
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'date_and_time' => 'required',
            'message' => 'string|max:120'
        ]);


        $reservation = Reservation::create($data);
        if( $reservation) {
            Toastr::success('Reservation sent successfully', 'Message', ["positionClass" => "toast-top-center"]);
            return redirect()->back();
        } else {
            Toastr::error('Reservation Is Not Been Sent', 'Message', ["positionClass" => "toast-top-center"]);
            return redirect()->back();
        }

    }
}
