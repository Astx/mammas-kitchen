<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $contacts = Contact::all();
        return view('admin.contact.index', compact('contacts'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        if($contact){
         return view('admin.contact.show', compact('contact'));
        }
        return 'Error';
    }

    /**
     * @param $id
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);

        if ($contact) {
            $delete = $contact->delete();

            if ($delete) {
                Toastr::success('Message deleted successfully', 'Message', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => true
                ], 200);
            } else {
                Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => false
                ], 404);
            }
        } else {
            Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

            return response([
                'status' => false
            ], 404);
        }

    }
}
