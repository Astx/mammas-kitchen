<?php

namespace App\Http\Controllers\Admin;

use App\Reservation;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    public function index()
    {
        $reservations = Reservation::all();
        return view('admin.reservation.index', compact('reservations'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status($id)
    {
        $reservations = Reservation::find($id);

        if ($reservations) {
            if($reservations->status == false) {
                $reservations->status = true;
                $reservations->save();
                Toastr::success('Reservation deleted successfully', 'Message', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => true
                ], 200);
            } else {
                $reservations->status = false;
                $reservations->save();
                Toastr::success('Reservation deleted successfully', 'Message', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => true
                ], 200);
            }
        } else {
            Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

            return response([
                'status' => false
            ], 404);
        }


//        Toastr::success('Reservation confirmed successfully', 'Message', ["positionClass" => "toast-top-right"]);
//        return redirect()->back();

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $reservation = Reservation::find($id);

        if ($reservation) {
            $delete = $reservation->delete();

            if ($delete) {
                Toastr::success('Reservation deleted successfully', 'Message', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => true
                ], 200);
            } else {
                Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => false
                ], 404);
            }
        } else {
            Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

            return response([
                'status' => false
            ], 404);
        }

    }


}
