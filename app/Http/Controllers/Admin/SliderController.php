<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{

    public function index()
    {
        $sliders = Slider::all();
        return view('admin.slider.index', compact('sliders'));
    }


    public function create()
    {
        return view('admin.slider.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:50|string',
            'sub_title' => 'required|max:50|string',
            'image' => 'required|mimes:jpeg,png,jpg'
        ]);

        $data = $request->all();
        $image = $request->file('image');
        $slug = str_slug('$request', 'title');
        if (isset($image)) {
            $CurrentDate = now()->toDateString();
            $image_name = $slug . '-' . $CurrentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/slider')) {
                mkdir('uploads/slider', 0777, true);
            }
            if (!$image->move(public_path('/uploads/slider'), $image_name)) {
                return redirect()->back()
                    ->with('error', 'The image is not moved into folder.');
            }
        } else {
            $image_name = 'default.png';
        }

        $data['image'] = $image_name;
        $slider = Slider::create($data);
        if ($slider) {
            return redirect()->route('slider.index')->with('successMsg', 'Slider Successfully Saved.');
        } else {
            return redirect()->route('slider.index')->with('errorMsg', 'Slider Does Not Saved.');
        }

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $slider = Slider::find($id);
        if($slider) {
            return view('admin.slider.edit', compact('slider'));
        }else {
            return 'Error';
        }

    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:50|string',
            'sub_title' => 'required|max:50|string',
            'image' => 'mimes:jpeg,png,jpg'
        ]);

        $image = $request->file('image');
        $slug = str_slug('$request', 'title');
        $slider = Slider::find($id);
        if (isset($image)) {
            $CurrentDate = now()->toDateString();
            $imagename = $slug . '-' . $CurrentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/slider')) {
                mkdir('uploads/slider', 0777, true);
            }

            if (!$image->move(public_path('/uploads/slider'), $imagename)) {
                return redirect()->back()
                    ->with('error', 'The image is not moved into folder.');
            }
        } else {
            $imagename = $slider->image;
        }

        $slider->title = $request->title;
        $slider->sub_title = $request->sub_title;
        $slider->image = $imagename;
        $slider->update();
        if($slider) {
            return redirect()->route('slider.index')->with('successMsg', 'Slider Successfully Updated.');
        }
            return redirect()->route('slider.index')->with('errorMsg', 'Slider Does Not Updated.');
    }


    public function destroy($id)
    {
        $slider = Slider::find($id);

        if ($slider) {
            unlink('uploads/slider/' . $slider->image);
            $delete = $slider->delete();

            if ($delete) {
                Toastr::success('Reservation deleted successfully', 'Message', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => true
                ], 200);
            } else {
                Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => false
                ], 404);
            }
        } else {
            Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

            return response([
                'status' => false
            ], 404);
        }

    }



}
