<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.category.index', compact('categories'));
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.category.create');
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|max:50|string'
        ]);
        $data['slug'] = str_slug($data['name']);
        $category = Category::create($data);
        if($category){
            return redirect()->route('category.index')->with('successMsg', 'Category Successfully Saved.');
        } else {
            return redirect()->route('category.index')->with('errorMsg', 'Category Does Not Saved.');
        }

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $category = Category::find($id);

        return view('admin.category.edit', compact('category'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:50|string'
        ]);
        $category = Category::find($id);
        $category->name = $request->name;
        $category->slug = str_slug($request->name);
        $category->update();
        if($category) {
            return redirect()->route('category.index')->with('successMsg', 'Category Successfully Updated.');
        }
            return redirect()->route('category.index')->with('errorMsg', 'Category Does Not Updated.');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        $category = Category::find($id);

        if ($category) {
            $delete = $category->delete();

            if ($delete) {
                Toastr::success('Reservation deleted successfully', 'Message', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => true
                ], 200);
            } else {
                Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => false
                ], 404);
            }
        } else {
            Toastr::error('ID you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);

            return response([
                'status' => false
            ], 404);
        }


    }
}
