<?php

namespace App\Http\Controllers\Admin;

use App\Item;
use App\Category;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{

    public function index()
    {
        $items = Item::all();
        return view('admin.item.index', compact('items'));
    }


    public function create()
    {
        $categories = Category::all();
        return view('admin.item.create', compact('categories'));
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'category_id' =>'required|max:50|string',
            'name' =>'required|max:50|string',
            'description' =>'required|max:120|string',
            'price' =>'required|numeric',
            'image' => 'required|mimes:jpeg,png,jpg'
        ]);
        $image = $request->file('image');
        $slug = str_slug('$request', 'name');
        if(isset($image)) {
            $CurrentDate = now()->toDateString();
            $imagename = $slug.'-'. $CurrentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!file_exists('uploads/slider'))
            {
                mkdir('uploads/item', 0777, true);
            }
            if (!$image->move(public_path('/uploads/item'), $imagename)) {
                return redirect()->back()
                    ->with('error', 'The image is not moved into folder.');
            }
        } else
        {
            $imagename = 'default.png';
        }
        $data['image'] = $imagename;
        $item = Item::create($data);
        if($item) {
            return redirect()->route('item.index')->with('successMsg', 'Item Successfully Saved.');
        } else {
            return redirect()->route('item.index')->with('error', 'Item Does Not Saved.');
        }

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $item = Item::find($id);
        if($item){
            $categories = Category::all();
            return view('admin.item.edit', compact('item', 'categories'));
        }else {
            return 'Error';
        }
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' =>'required|max:50|string',
            'name' =>'required|max:50|string',
            'description' =>'required|max:120|string',
            'price' =>'required|numeric',
            'image' => 'mimes:jpeg,png,jpg'
        ]);

        $item = Item::find($id);
        if($item){
            $image = $request->file('image');
            $slug = str_slug('$request','name');
            $item = Item::find($id);
            if (isset($image)) {
                $CurrentDate = now()->toDateString();
                $imagename = $slug.'-'. $CurrentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
                if(!file_exists('uploads/item'))
                {
                    unlink('uploads/item/'.$item->image);
                    mkdir('uploads/item', 0777, true);
                }

                if (!$image->move(public_path('/uploads/item'), $imagename)) {
                    return redirect()->back()
                        ->with('error', 'The image is not moved into folder.');
                }
            } else {
                $imagename = $item->image;
            }

            $item->category_id = $request->category;
            $item->name = $request->name;
            $item->description = $request->description;
            $item->price = $request->price;
            $item->image = $imagename;
            $item->update();
            return redirect()->route('item.index')->with('successMsg', 'Item Successfully Updated.');
        }else {
            return redirect()->route('item.index')->with('errorMsg', 'ID you are looking for is not found.');
        }

    }


    public function destroy($id)
    {
        $item = Item::find($id);

        if ($item) {
            if (file_exists('uploads/item/'.$item->image)){
                unlink('uploads/item/'.$item->image);
            }
            $delete = $item->delete();

            if ($delete) {
                Toastr::success('Reservation deleted successfully', 'Message', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => true
                ], 200);
            } else {
                Toastr::error('Item you are looking for is not.', 'Error', ["positionClass" => "toast-top-right"]);

                return response([
                    'status' => false
                ], 404);
            }
        } else {
            Toastr::error('Items you are looking for is not found.', 'Error', ["positionClass" => "toast-top-right"]);
            return response([
                'status' => false
            ], 404);
        }

    }

}
