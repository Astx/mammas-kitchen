<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\SendMessageRequest;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function sendMessage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50|string',
            'email' => 'required|email',
            'subject' => 'required|max:50|string',
            'message' => 'required|max:120|string'
        ]);
        $contact = Contact::create($request->all());
        if($contact){
            Toastr::success('Your message successfully sent', 'Success', ["positionClass" => "toast-top-center"]);
            return redirect()->back();
        } else {
            Toastr::error('Your message successfully sent', 'Success', ["positionClass" => "toast-top-center"]);
            return redirect()->back();
        }

    }
}
