@extends('layouts.app')

@section('title', 'Category')

@push('css')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none'">&times;</button>
                                <span>{{ $error }} </span>
                            </div>
                        @endforeach
                    @endif
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Update Category</h4>

                        </div>
                        <div class="card-content">
                            <form method="POST" action="{{ route('category.update', $category->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="name">Name</label>
                                            <input type="text" class="form-control" name="name" value="{{ $category->name }}" id="name">
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ route('category.index') }}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 800);
    </script>


@endpush