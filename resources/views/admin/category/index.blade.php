@extends('layouts.app')

@section('title', 'Category')

@push('css')
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush

@section('content')
   <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('successMsg'))
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none'">&times;</button>
                                        <span> {{ session('successMsg') }} </span>
                                </div>
                             @endif
                            <a href="{{ route('category.create') }}" class="btn btn-info">Add New</a>
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">All Categories</h4>
                                
                                </div>
                                <div class="card-content table-responsive">
                                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead class="text-primary">
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Slug</th>
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($categories as $key=>$category)
                                                <tr data-id="{{ $category->id }}">
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $category->name }}</td>
                                                    <td>{{ $category->slug }}</td>
                                                    <td>{{ $category->created_at }}</td>
                                                    <td>{{ $category->updated_at }}</td>
                                                    <td>
                                                        <a href="{{ route('category.edit', $category->id) }}" class="btn btn-info btn-sm"><i class="material-icons">mode_edit</i></a>

                                                        {{--<form method='POST' id="form-delete-{{ $category->id }}" action="{{ route('category.destroy', $category->id) }}" style="display:none;">--}}
                                                            {{--@csrf--}}
                                                            {{--@method('DELETE')--}}
                                                        {{--</form>--}}
                                                        <button type="button" class="btn btn-danger btn-sm delete-btn" data-id="{{ $category->id }}"><i class="material-icons">delete</i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                        
                </div>
         </div>
   </div>
    @include('modals.confirm')
@endsection

@push('scripts') 
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="{{ asset('backend/js/modal.js') }}" type="text/javascript"></script>
    <script>
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 800);
    </script>
@endpush