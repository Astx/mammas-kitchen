@extends('layouts.app')

@section('title', 'Item')

@push('css')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger">
                                <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none'">&times;</button>
                                        <span>{{ $error }} </span>
                            </div>
                        @endforeach
                    @endif
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Update Item</h4>

                        </div>
                        <div class="card-content">
                            <form method="POST" action="{{ route('item.update', $item->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Category Name</label>
                                            <select class="form-control" name="category">
                                                @foreach($categories as $category)
                                                    <option {{ $category->id == $item->category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="name">Name</label>
                                            <input type="text" class="form-control" id="name" value="{{ $item->name }}" name="name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="description">Description</label>
                                            <textarea  class="form-control" name="description" id="description">{{ $item->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="price">Price</label>
                                            <input type="number" class="form-control" name="price" value="{{ $item->price }}" id="price">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="image">Image</label>
                                        <input type="file"  name="image" id="image">
                                        <img src="{{ asset('uploads/item/'.$item->image) }}" style="width:250px;" alt="slider image">
                                    </div>
                                </div>
                                <a href="{{ route('item.index') }}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 800);
    </script>

@endpush