@extends('layouts.app')

@section('title', 'Items')

@push('css')
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush

@section('content')
   <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('successMsg'))
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none'">&times;</button>
                                        <strong>{{ session('successMsg') }} </strong>
                                </div>
                             @endif
                            <a href="{{ route('item.create') }}" class="btn btn-info">Add New</a>
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">All Categories</h4>
                                
                                </div>
                                <div class="card-content table-responsive">
                                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead class="text-primary">
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Category Name</th>
                                                <th>Description</th>
                                                <th>Price</th>
                                                <th>Created At</th>
                                                <th>Updated At</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($items as $key=>$item)
                                                <tr data-id="{{ $item->id }}">
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $item->name }}</td>
                                                    <td style="width:30%"><img class="img-responsive img-thumbnail" id="item-image" src="{{ asset('uploads/item/'.$item->image) }}" alt="image" style="width:300px;margin-left:80px;"></td>
                                                    <td>{{ $item->category->name}}</td>
                                                    <td>{{ $item->description }}</td>
                                                    <td>{{ $item->price }}</td>
                                                    <td>{{ $item->created_at }}</td>
                                                    <td>{{ $item->updated_at }}</td>
                                                    <td>
                                                        <a href="{{ route('item.edit', $item->id) }}" class="btn btn-info btn-sm"><i class="material-icons">mode_edit</i></a>
                                                        <button type="button" class="btn btn-danger btn-sm delete-btn" data-id="{{$item->id}}"><i class="material-icons">delete</i></button>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                        
                </div>
         </div>
   </div>
    @include('modals.confirm')
@endsection

@push('scripts') 
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="{{ asset('backend/js/modal.js') }}" type="text/javascript"></script>
    <script>
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 800);
    </script>
@endpush