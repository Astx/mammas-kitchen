@extends('layouts.app')

@section('title', 'Slider')

@push('css')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if($errors->any())
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger">
                                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none'">&times;</button>
                                        <span> {{ $error }} </span>
                                    </div>
                                @endforeach
                     @endif
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Add New Slider</h4>

                        </div>
                        <div class="card-content">
                            <form method="POST" action="{{ route('slider.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="title">Title</label>
                                            <input type="text" class="form-control" name="title" value="{{ old('title') }}" id="title">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="sub-title">Sub Title</label>
                                            <input type="text" class="form-control" name="sub_title" value="{{ old('sub_title') }}" id="sub-title">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                            <label class="control-label" for="image">Image</label>
                                            <input type="file"  name="image" id="image">
                                    </div>
                                </div>
                                <a href="{{ route('slider.index') }}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
         window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
             });
         }, 800);
    </script>

@endpush
