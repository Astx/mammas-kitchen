@extends('layouts.app')

@section('title', 'Reservation')

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session('successMsg'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none'">&times;</button>
                            <span>{{ session('successMsg') }}</span>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">All Reservations</h4>

                        </div>
                        <div class="card-content table-responsive">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead class="text-primary">
                                     <tr>
                                         <th>ID</th>
                                         <th>Name</th>
                                         <th>Phone</th>
                                         <th>Email</th>
                                         <th>Date and Time</th>
                                         <th>Message</th>
                                         <th>Status</th>
                                         <th>Created At</th>
                                         <th>Action</th>
                                     </tr>
                                </thead>
                                <tbody>
                                @if($reservations->isNotEmpty())
                                    @foreach($reservations as $key => $reservation)
                                        <tr data-id="{{ $reservation->id }}" class="reservation-row">
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $reservation->name }}</td>
                                            <td>{{ $reservation->phone }}</td>
                                            <td>{{ $reservation->email }}</td>
                                            <td>{{ $reservation->date_and_time }}</td>
                                            <td>{{ $reservation->message }}</td>
                                            <td id="label-info-{{ $reservation->id }}">
                                                @if ($reservation->status == true)
                                                    <span data-id="{{  $reservation->id }}" class="confirmation label label-info">Confirmed</span>
                                                @else
                                                    <span data-id="{{  $reservation->id }}" class="confirmation label label-danger">Not Confirmed Yet</span>
                                                @endif
                                            </td>
                                            <td>{{ $reservation->created_at }}</td>
                                            <td >
                                                @if ($reservation->status == false)
                                                    <form method='POST' id="form-status-{{ $reservation->id }}" action="{{ route('reservation.status', $reservation->id) }}" style="display:none;">
                                                        @csrf
                                                    </form>
                                                    <button type="button" class="btn btn-info btn-sm confirm-btn" data-id="{{ $reservation->id }}"><i class="material-icons">done</i></button>
                                                    <button type="button" class="btn btn-success btn-sm reset-btn" data-id="{{ $reservation->id }}" style="display:none;"><i class="material-icons">star</i></button>
                                                @else
                                                    <form method='POST' id="form-status-{{ $reservation->id }}" action="{{ route('reservation.status', $reservation->id) }}" style="display:none;">
                                                        @csrf
                                                    </form>
                                                    <button type="button" class="btn btn-success btn-sm reset-btn" data-id="{{ $reservation->id }}"><i class="material-icons">star</i></button>
                                                    <button type="button" class="btn btn-info btn-sm confirm-btn" data-id="{{ $reservation->id }}" style="display:none;"><i class="material-icons">done</i></button>
                                                @endif

                                                    <form method='POST' id="form-delete-{{ $reservation->id }}" action="{{ route('reservation.destroy', $reservation->id) }}" style="display:none;">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                    <button type="button" class="btn btn-danger btn-sm delete-btn" data-id="{{ $reservation->id }}"><i class="material-icons">delete</i></button>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('modals.confirm')
    @include('modals.edit')
    @include('modals.resetConfirmation')
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="{{ asset('backend/js/modal.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/js/edit-modal.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/js/reset-confirmation.js') }}" type="text/javascript"></script>
    <script>
         window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
         }, 800);
    </script>
@endpush