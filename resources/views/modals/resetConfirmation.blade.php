<div class="modal fade" id="resetConfirm">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Have you already confirmed the reservation?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="reset-reserve">Yes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="reset">No</button>
            </div>

        </div>
    </div>
</div>