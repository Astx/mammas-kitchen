<div class="modal fade" id="myModal">
    <div class="modal-dialog">
<div class="modal-content">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        Are you sure? You want to delete this item?
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-success" id="confirm">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="reset">No</button>
    </div>

</div>
</div>
</div>

<div class="modal fade" id="myModalError">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Item You Are Looking For Is Not Found!
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="back">Back</button>
            </div>

        </div>
    </div>
</div>